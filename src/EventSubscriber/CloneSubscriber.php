<?php

namespace Drupal\entity_clone_tr\EventSubscriber;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldConfigInterface;

/**
 * Class CloneSubscriber.
 */
class CloneSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events['entity_clone.post_clone'] = ['entityClonePostClone'];

    return $events;
  }

  /**
   * This method is called when the entity_clone.post_clone is dispatched.
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   The dispatched event.
   * 
   * @return void
   */
  public function entityClonePostClone(EntityCloneEvent $event) {
    $cloned_entity = $event->getClonedEntity();
    $this->clearLanguages($cloned_entity);
    $original_entity = $event->getEntity();
    //$this->processReferences($cloned_entity);
    $original_references = $this->getAllReferences($original_entity);
    $cloned_references   = $this->getAllReferences($cloned_entity);
    foreach ($cloned_references as $cloned) {
      $cloned_id = $cloned->id();
      $og        = false;
      foreach ($original_references as $original) {
        if ($cloned_id == $original->id()) {
          $og = true;
        }
      }
      if (!$og) {
        $this->clearLanguages($cloned);
      }
    }
  }

  /**
   * Unused method for processing referenced entities.
   * 
   * @param \Drupal\Core\Entity\EntityInterface $cloned_entity
   * 
   * @return void
   */
  protected function processReferences(EntityInterface $cloned_entity) {
    $this->clearLanguages($cloned_entity);
    if ($cloned_entity instanceof FieldableEntityInterface) {
      foreach ($cloned_entity->getFieldDefinitions() as $field_id => $field_definition) {
        if ($this->fieldIsClonable($field_definition)) {
          $field = $cloned_entity->get($field_id);
          if ($field->count() > 0) {
            foreach ($field as $value) {
              $referenced_entity = $value->get('entity')->getTarget()->getValue();
              $this->processReferences($referenced_entity);
            }
          }
        }
      }
    }
  }

  /**
   * Collecting all referenced entities of an entity recursively.
   * 
   * @param \Drupal\Core\Entity\EntityInterface $cloned_entity
   * 
   * @return array
   */
  protected function getAllReferences(EntityInterface $cloned_entity) {
    $references = [$cloned_entity];
    if ($cloned_entity instanceof FieldableEntityInterface) {
      foreach ($cloned_entity->getFieldDefinitions() as $field_id => $field_definition) {
        if ($this->fieldIsClonable($field_definition)) {
          $field = $cloned_entity->get($field_id);
          if ($field->count() > 0) {
            $field_references = [];
            foreach ($field as $value) {
              $referenced_entity = $value->get('entity')->getTarget()->getValue();
              $field_references  = $this->getAllReferences($referenced_entity);
            }
            if (count($field_references) != 0) {
              $references = array_merge($references, $field_references);
            }
          }
        }
      }
    }
    return $references;
  }

  /**
   * Removes every translation that is not
   * the cloned entity's translation language
   * 
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * 
   * @return void
   */
  protected function clearLanguages(EntityInterface $entity) {
    if ($entity instanceof ContentEntityBase) {
      $languages = $entity->getTranslationLanguages();
      foreach ($languages as $language) {
        $lang_id             = $language->getId();
        $translation         = $entity->getTranslation($lang_id);
        $untranslated_entity = $translation->getUntranslated();
        $langcode            = $untranslated_entity->language()->getId();
        if ($lang_id != $langcode) {
          $untranslated_entity->removeTranslation($lang_id);
          $untranslated_entity->save();
        }
      }
    }
  }

  /**
   * Determines if a field is clonable.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   TRUE if the field is clonable; FALSE otherwise.
   */
  protected function fieldIsClonable(FieldDefinitionInterface $field_definition) {
    $clonable_field_types = [
      'entity_reference',
      'entity_reference_revisions',
    ];

    $type_is_clonable = in_array($field_definition->getType(), $clonable_field_types, TRUE);
    if (($field_definition instanceof FieldConfigInterface) && $type_is_clonable) {
      return TRUE;
    }
    return FALSE;
  }

}
